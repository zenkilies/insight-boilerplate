import React from "react";

import Sidebar from "./Sidebar";

export default function Layout({children, ...props}) {
  return (
    <section {...props}>
      <div className="container-fluid">
        <div className="row">
          <Sidebar/>

          <div className="col">
            {children}
          </div>
        </div>
      </div>
    </section>
  );
}
