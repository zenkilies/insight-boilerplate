import Link from "next/link";
import React from "react";
import {useRouter} from "next/router";

export default function Sidebar() {
  return (
    <section className="sidebar">
      <h1 className="menu-brand">
        Insight
      </h1>

      <MenuItemHeader title={"General"}/>
      <MenuItem href={"/"} title={"Dashboard"}/>
      <MenuItem href={"/about"} title={"About"}/>

      <MenuItemHeader title={"Helpers"}/>
      <MenuItem href={"/helpers/timestamp-converter"} title={"Timestamp Converter"}/>
    </section>
  );
}

declare interface IMenuItemProps {
  href: string;
  title: string;
}

function MenuItem(props: IMenuItemProps) {
  const router = useRouter();

  const classNames = ["menu-item"];
  if (router.pathname === props.href) {
    classNames.push("active");
  }

  return (
    <div className={classNames.join(" ")}>
      <Link href={props.href}>
        <a>{props.title}</a>
      </Link>
    </div>
  );
}

declare interface IMenuItemHeaderProps {
  title: string;
}

function MenuItemHeader(props: IMenuItemHeaderProps) {
  return (
    <div className="menu-item menu-item-header">
      {props.title}
    </div>
  );
}
