import Layout from "src/components/layout/Layout";

import "src/styles/app.scss";

export default function MyApp({Component, pageProps}) {
  return (
    <Layout>
      <Component {...pageProps} />
    </Layout>
  );
}
