import React, {useState} from "react";
import moment from "moment";

export default function IndexPage() {
  return (
    <div className="container-fluid mt-5">
      <div className="row">
        <div className="col-6">
          <UnixTimestampToDateTime/>
        </div>

        <div className="col-6">
          <DateTimeToUnixTimestamp/>
        </div>
      </div>
    </div>
  );
}

function UnixTimestampToDateTime() {
  const [timestamp, setTimestamp] = useState(0);

  let result = <React.Fragment/>;
  if (timestamp > 0) {
    let date = new Date(timestamp * 1000);
    let dateMoment = moment(timestamp * 1000);

    result = (
      <div>
        <p>
          <strong>GMT:</strong> {dateMoment.utc().format()}
        </p>

        <p>
          <strong>Local:</strong> {dateMoment.local().format()}
        </p>
      </div>
    );
  }

  return (
    <div className="card">
      <div className="card-body">
        <h6 className="mb-4">
          Unix Timestamp to Date Time
        </h6>

        <div className="mb-3">
          <label className="form-label">Unix Timestamp</label>
          <input type="number" className="form-control" placeholder="1577836800" onChange={e => setTimestamp(e.target.value)}/>
        </div>

        {result}
      </div>
    </div>
  );
}

function DateTimeToUnixTimestamp() {
  const [year, setYear] = useState(2020);
  const [month, setMonth] = useState(12);
  const [day, setDay] = useState(31);
  const [hour, setHour] = useState(23);
  const [minute, setMinute] = useState(59);
  const [second, setSecond] = useState(59);

  let result = <React.Fragment/>;
  let dateMoment = moment({
    year: year,
    month: month - 1,
    day: day,
    hour: hour,
    minute: minute,
    second: second,
    millisecond: 0,
  });

  result = (
    <div>
      <p>GMT Unix Timestamp: {dateMoment.utc().unix()}</p>
    </div>
  );

  return (
    <div className="card">
      <div className="card-body pb-0">
        <h6 className="mb-4">
          Date Time to Unix Timestamp
        </h6>

        <div className="row mb-2">
          <div className="col">
            <label className="form-label">Year</label>
            <input type="number" className="form-control" placeholder="2020" value={year} onChange={e => setYear(e.target.value)}/>
          </div>

          <div className="col">
            <label className="form-label">Month</label>
            <input type="number" className="form-control" placeholder="12" value={month} onChange={e => setMonth(e.target.value)}/>
          </div>

          <div className="col">
            <label className="form-label">Day</label>
            <input type="number" className="form-control" placeholder="31" value={day} onChange={e => setDay(e.target.value)}/>
          </div>

          <div className="col">
            <label className="form-label">Hour</label>
            <input type="number" className="form-control" placeholder="23" value={hour} onChange={e => setHour(e.target.value)}/>
          </div>

          <div className="col">
            <label className="form-label">Min</label>
            <input type="number" className="form-control" placeholder="59" value={minute} onChange={e => setMinute(e.target.value)}/>
          </div>

          <div className="col">
            <label className="form-label">Sec</label>
            <input type="number" className="form-control" placeholder="59" value={second} onChange={e => setSecond(e.target.value)}/>
          </div>
        </div>

        {result}
      </div>
    </div>
  );
}
