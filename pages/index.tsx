import React from "react";

export default function IndexPage() {
  return (
    <div className="container mt-5">
      <p>Hello, this is <strong>Dashboard</strong> page!</p>
    </div>
  );
}
